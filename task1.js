const fs = require('fs')

function loadJSON(filename) {

    return JSON.parse(
        fs.existsSync(filename)
            ? fs.readFileSync(filename)
            : null
    )
}

function saveJSON(json, filename) {
    return fs.writeFileSync(filename, JSON.stringify(json, null, 2))
}

function reverseNames(arr) {

    let result = []

    arr.forEach(function (elm) {
        let name = elm.split('').reverse().join('')
        result.push(name.toLowerCase())
    })
    return result
}

//function to set email format (name + 5 random characters + '@gmail.com')
function getEmail(arr) {

    let result = {
        emails: []
    }

    arr.forEach(function (elm) {
        let email = elm + Math.random().toString(36).substring(2, 7) + '@gmail.com'
        result.emails.push(email)
    })
    return result
}

const data = loadJSON('input2.json')

saveJSON(getEmail(reverseNames(data.names)), 'output2.json')